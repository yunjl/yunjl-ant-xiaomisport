 ### 利用ant实现本地每天自动刷运动步数(Java)
## 前言
&ensp;&ensp;&ensp;当然有更好的云部署的办法，想实现云函数部署的论坛搜都有，我看没有关于用ant的，这个仅供java学习

首先需要手机下载小米运动App，注册账号，绑定微信运动，
然后需要从官网下载apache ant，地址为： http://ant.apache.org/bindownload.cgi ；安装比较简单，就是解压配置环境变量就好了和安装jdk差不多。（在环境变量配置窗口中的“用户变量”中新增一个变量名为ANT_HOME，值为Ant解压后的目录，然后在“用户变量”下找PATH变量，如果没有就新增一个PATH变量，如果有就直接在PATH变量中加入新的值，值为“%ANT_HOME%\bin”最后在cmd中执行ant -version测试）

	&ensp;&ensp;&ensp;ant 是一个将软件编译、测试、部署等步骤联系在一起加以自动化的一个工具，大多用于Java环境中的软件开发。有兴趣的可以去了解下：https://www.w3cschool.cn/ant/4xdp1hw8.html

不懂的也没关系，按照我下面说的做就行了：
源码是我在论坛下的：https://www.52pojie.cn/forum.php?mod=viewthread&tid=1584284&highlight=%D0%A1%C3%D7%D4%CB%B6%AF

&ensp;&ensp;&ensp;这个代码在项目中直接运行也是可以的，只是不能实现每天自动运行。
## 项目结构与配置
&ensp;&ensp;&ensp;我对这个代码进行了微改，这是我的完整项目结构：就是一个非常简单的maven项目，项目我放最后了。
![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/项目结构.png?x-oss-process=image/resize,p_100/quality,q_50)

里面就三个文件：源码就不说了
	build.xml 是ant的默认运行文件，看下里面的内容：
	![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/ant默认文件.png?x-oss-process=image/resize,p_100/quality,q_50)
	
```
<?xml version="1.0" encoding="UTF-8" ?>
<project name="XiaoMiSportUtils" default="run" basedir="">

    <!--运行 运行命令其实就是Java的命令。指明要运行的类名，以及路径，可以知名运行的class-->
    <target name="run">
        <java classname="yunjl.xiaomisport.XiaoMiSportUtils" classpath="D:\Java\IdeaProjects\new-ew-cloud\yunjl-ant-xiaomisport\target\classes;D:\Java\Maven\repository\cn\hutool\hutool-all\5.7.20\hutool-all-5.7.20.jar" />
    </target>

</project>
```
这里面有几点要注意的地方：

1、你只需要改的地方就是classname和classpath里面的值

2、classname里面的值是项目里的类名（需要带项目路径，不带后缀java）
	&ensp;&ensp;&ensp;这里我的XiaoMiSportUtils类的绝对路径是：`D:\Java\IdeaProjects\new-ew-cloud\yunjl-ant-xiaomisport\src\main\java\yunjl\xiaomisport\XiaoMiSportUtils.java`
因为是在项目里，且项目的package是 yunjl.xiaomisport ，所以我的classname的值就是yunjl.xiaomisport.XiaoMiSportUtils，你们要是自己建项目的话这个要注意，要不然后面运行类的时候会报错找不到这个类
![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/小米类.png?x-oss-process=image/resize,p_80/quality,q_50)

3、classpath的值就是java类编译后（.class）的绝对路径了（不是java类的）最简单的办法就是在项目中运行一下，会出现这个.class文件

![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/class.png?x-oss-process=image/resize,p_80/quality,q_50)

复制这个文件的绝对路径，去掉package路径，我的绝对路径是：`D:\Java\IdeaProjects\new-ew-cloud\yunjl-ant-xiaomisport\target\classes\yunjl\xiaomisport\XiaoMiSportUtils.class`
所以我填的值就是：`D:\Java\IdeaProjects\new-ew-cloud\yunjl-ant-xiaomisport\target\classes`

4、因为这个项目引入了hutool包，所以运行的时候也要指定jar包的绝对路径位置：在classpath值中填就行了，以；分隔

到这就已经成功一大半了，可以测试下；进入build.xml文件的目录，cmd打开命令行窗口，输入ant回车

![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/路径.png?x-oss-process=image/resize,p_70/quality,q_50)

&ensp;&ensp;&ensp;出现刷入成功就代表成功了，可以去微信运动查看。

![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/刷入成功.png?x-oss-process=image/resize,p_80/quality,q_50)

&ensp;&ensp;&ensp;成功的话改下bat文件：
```
ant -f D:\Java\IdeaProjects\xx-SpringCloud\xx-ant\src\main\java\yunjl\xioamisport\build.xml>D:\Java\IdeaProjects\xx-SpringCloud\xx-ant\src\main\java\yunjl\xioamisport\run.log
```
&ensp;&ensp;&ensp;用自己的build.xml文件的绝对路径替换掉我的，后面的路径就是一个输出日志，随便搞个位置都行，我放在了本层目录，运行后就会生成一个log文件

![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/log.png?x-oss-process=image/resize,p_100/quality,q_50)

## 定时任务
利用ant运行了java代码，下面就说一下怎么用windows的定时任务
win搜索任务计划程序

![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/任务计划.png?x-oss-process=image/resize,p_80/quality,q_50)

&ensp;&ensp;&ensp;右键创建一个基本任务，前面一下简单的配置
来到操作，选启动程序
![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/任务.png?x-oss-process=image/resize,p_70/quality,q_50)

选择bat文件

![](https://yunjl1.oss-cn-beijing.aliyuncs.com/tc/程序.png?x-oss-process=image/resize,p_70/quality,q_50)
下一步就可以了
## 项目源码
项目源码自行下载：
gitee：https://gitee.com/yunjl/yunjl-ant-xiaomisport

百度云：
&ensp;&ensp;&ensp;链接：https://pan.baidu.com/s/1Z9jVIh5DKYqpVR1kSs3d6Q?pwd=52pj 
&ensp;&ensp;&ensp;提取码：52pj 
&ensp;&ensp;&ensp;--来自百度网盘超级会员V4的分享